package com.n47.demo.hystrix;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HystrixController {

    @Autowired
    private HystrixService hystrixService;

    @GetMapping("/hystrix-example")
    public String testHystrix(@RequestParam("message") String message) throws InterruptedException {
        return hystrixService.testHystrix(message);
    }
}
