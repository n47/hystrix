package com.n47.demo.hystrix;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class HystrixService {

    @HystrixCommand(fallbackMethod = "fallbackHystrix",
            commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000")})
    public String testHystrix(String message) throws InterruptedException {
        Thread.sleep(4000);
        return message != null ? message : "Message is null";
    }

    public String fallbackHystrix(String message) {
        log.error("Request took to long. Timeout limit: 2000ms.");
        return "Request took to long. Timeout limit: 2000ms. Message: " + message;
    }
}
